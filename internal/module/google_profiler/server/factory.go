package server

import (
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/google_profiler"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modserver"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modshared"
)

type Factory struct {
}

func (f *Factory) New(config *modserver.Config) (modserver.Module, error) {
	return &module{
		cfg:     config.Config.Observability.GoogleProfiler,
		service: config.KASName,
		version: config.Version,
	}, nil
}

func (f *Factory) Name() string {
	return google_profiler.ModuleName
}

func (f *Factory) StartStopPhase() modshared.ModuleStartStopPhase {
	return modshared.ModuleStartBeforeServers
}
