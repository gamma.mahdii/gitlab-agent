package modshared

import (
	"context"

	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware/v2"
	"google.golang.org/grpc"
)

type rpcAPIKeyType int

const (
	rpcAPIKey rpcAPIKeyType = iota
)

type RPCAPIFactory func(ctx context.Context, method string) RPCAPI

func InjectRPCAPI(ctx context.Context, rpcAPI RPCAPI) context.Context {
	return context.WithValue(ctx, rpcAPIKey, rpcAPI)
}

func RPCAPIFromContext(ctx context.Context) RPCAPI {
	rpcAPI, ok := ctx.Value(rpcAPIKey).(RPCAPI)
	if !ok {
		// This is a programmer error, so panic.
		panic("RPCAPI not attached to context. Make sure you are using interceptors")
	}
	return rpcAPI
}

// UnaryRPCAPIInterceptor returns a new unary server interceptor that augments connection context with a RPCAPI.
func UnaryRPCAPIInterceptor(factory RPCAPIFactory) grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
		return handler(augmentContextWithRPCAPI(ctx, info.FullMethod, factory), req)
	}
}

// StreamRPCAPIInterceptor returns a new stream server interceptor that augments connection context with a RPCAPI.
func StreamRPCAPIInterceptor(factory RPCAPIFactory) grpc.StreamServerInterceptor {
	return func(srv interface{}, ss grpc.ServerStream, info *grpc.StreamServerInfo, handler grpc.StreamHandler) error {
		wrapper := grpc_middleware.WrapServerStream(ss)
		wrapper.WrappedContext = augmentContextWithRPCAPI(wrapper.WrappedContext, info.FullMethod, factory)
		return handler(srv, wrapper)
	}
}

func augmentContextWithRPCAPI(ctx context.Context, method string, factory RPCAPIFactory) context.Context {
	return InjectRPCAPI(ctx, factory(ctx, method))
}
